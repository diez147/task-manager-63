package ru.tsc.babeshko.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.service.model.IService;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.model.AbstractModel;
import ru.tsc.babeshko.tm.repository.model.AbstractRepository;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected abstract AbstractRepository<M> getRepository();

    @Override
    public void add(@NotNull final M model) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.deleteAll();
        repository.saveAll(models);
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.delete(model);
    }

    @Override
    public void clear() {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.deleteAll();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.deleteById(id);
    }

    @Override
    public long getCount() {
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.count();
    }

}